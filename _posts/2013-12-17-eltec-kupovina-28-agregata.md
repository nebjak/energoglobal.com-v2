---
layout: post
title: 'Eltec Export-Import doo'
date: 2013-12-17 12:00:00
categories: reference
excerpt_img: 2013/12/eltec.jpg
excerpt: Potpisan je ugovor sa distributerom Eltec Export-Import doo o isporuci 28 prenosnih dizel agregata.
tags: agregat, lombardini, linz
gallery:
  - 2013/12/eltec_1.jpg
  - 2013/12/eltec_2.jpg
  - 2013/12/eltec_3.jpg
  - 2013/12/eltec_4.jpg
---

Danas je potpisan ugovor sa distributerom Eltec Export-Import o isporuci
28 prenosnih dizel agregata, od čega 21 snage 8 kVA (LDA8003E) i 7 snage
12,5 kVA (LDA13003E).

Agregate čini Lombardini Antor - Linz motorgeneratorska grupa.
