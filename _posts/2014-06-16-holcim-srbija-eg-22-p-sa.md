---
layout: post
title: 'Holcim Srbija isporuka EG 22 P-SA'
date: 2014-06-16 13:37:00
categories: reference
excerpt_img: 2014/06/holcim.jpg
excerpt: Isporuka i puštanje u rad dizel agregata EG 22 P-SA, snage 22 kVA.
tags: agregat, perkins, linz
gallery:
  - 2014/06/holcim_1.jpg
  - 2014/06/holcim_2.jpg
  - 2014/06/holcim_3.jpg
  - 2014/06/holcim_4.jpg
  - 2014/06/holcim_5.jpg
  - 2014/06/holcim_6.jpg
---

U saradnji sa firmom Elmamont inženjering danas je isporučen i pušten u rad
dizel agregat Energoglobal EG 22 P-SA, snage 22 kVA, koji će vršiti funkciju
rezervnog izvora napajanja u kompaniji **Holcim Srbija**.

Agregat pokreće vrhunski Perkins 404A-22G dizel motor. Agregat je
smešten u zvučno izolovano kućište, i opremljen je za automatsko prebacivanje
mreža-agregat.
