---
layout: post
title: 'Best Izgradnja isporuka EG 150 P-SA'
date: 2014-06-18 12:00:00
categories: reference
excerpt_img: 2014/06/best.jpg
excerpt: Isporuka i puštanje u rad dizel agregata EG 150 P-SA, snage 150 kVA.
tags: agregat, perkins, meccalte
gallery:
  - 2014/06/best_1.jpg
  - 2014/06/best_2.jpg
  - 2014/06/best_3.jpg
---

Danas je isporučen i pušten u rad dizel agregat Energoglobal EG 150 P-SA,
snage 150 kVA, koji će vršiti funkciju rezervnog izvora napajanja u
_Centralnom skladištu i fabrici betona_ kompanije **Best Izgradnja**.

Agregat pokreće vrhunski Perkins 1006TAG dizel motor. Agregat je smešten
u zvučno izolovano kućište, i opremljen je za automatsko prebacivanje
mreža-agregat.
