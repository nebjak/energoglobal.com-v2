---
layout: post
title: 'Global Seed isporuka EG 15 L-OM'
date: 2014-06-19 13:37:00
categories: reference
excerpt_img: 2014/06/global_seed.jpg
excerpt: Isporuka i puštanje u rad dva dizel agregata EG 15 L-OM, snage 15 kVA.
tags: agregat, lombardini, linz
gallery:
  - 2014/06/global_seed_1.jpg
  - 2014/06/global_seed_2.jpg
  - 2014/06/global_seed_3.jpg
  - 2014/06/global_seed_4.jpg
  - 2014/06/global_seed_5.jpg
  - 2014/06/global_seed_6.jpg
---

Danas su isporučena i puštena u rad dva dizel agregata Energoglobal EG 15 L-OM,
snage 15 kVA, koji će vršiti funkciju rezervnog izvora napajanja na farmi
kompanije **Global Seed** u Čurugu. Agregat je isporučen u saradnji sa
distributerom Čavić & Prof.

Agregate pokreće vrhunski Lombardini LDW1603 dizel motor. Agregati su
otvorenog tipa, montirani na čeličnu bazu. Agregati su opremljeni za ručni
režim rada.
