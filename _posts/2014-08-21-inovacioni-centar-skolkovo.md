---
layout: post
title: 'Inovacioni centar Skolkovo'
date: 2014-08-21 13:37:00
categories: reference
excerpt_img: 2014/08/inovacioni_centar_skolkovo.jpg
excerpt: Isporuka i puštanje u rad dva dizel agregata snage 1320 kVA.
tags: agregat, volvo, sincro, rusija
gallery:
  - 2014/08/inovacioni_centar_skolkovo_1.jpg
  - 2014/08/inovacioni_centar_skolkovo_2.jpg
  - 2014/08/inovacioni_centar_skolkovo_3.jpg
  - 2014/08/inovacioni_centar_skolkovo_4.jpg
  - 2014/08/inovacioni_centar_skolkovo_5.jpg
  - 2014/08/inovacioni_centar_skolkovo_6.jpg
  - 2014/08/inovacioni_centar_skolkovo_7.jpg
  - 2014/08/inovacioni_centar_skolkovo_8.jpg
  - 2014/08/inovacioni_centar_skolkovo_9.jpg
  - 2014/08/inovacioni_centar_skolkovo_10.jpg
  - 2014/08/inovacioni_centar_skolkovo_11.jpg
  - 2014/08/inovacioni_centar_skolkovo_12.jpg
  - 2014/08/inovacioni_centar_skolkovo_13.jpg
  - 2014/08/inovacioni_centar_skolkovo_14.jpg
---

Stručni tim Energoglobal-a je isporučio i pustio u rad dizel električni agregat snage 1320 kVA na objekat "Inovacioni centar Skolkovo" (Инновационный центр «Сколково» / Skolkovo innovation center).

Dizel agregat se sastoji od dva agregata u jednom kućištu, a odlikuju ga sledeće karakteristike:

- Dizel agregat pokreću dva vrhunska Volvo TAD1642GE dizel motora upareni sa Sincro SK355LS generatorima
- Odlična cena u odnosu na ukupnu snagu agregata
- Pouzdan sinhronizovan rad i redudantnost
- Niži troškovi održavanje u odnosu na dizel agregat iste snage
- Mogućnost servisiranja jedne jedinice dok druga radi, tj. neprekidan rad agregata
