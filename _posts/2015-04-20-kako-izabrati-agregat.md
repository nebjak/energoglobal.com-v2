---
layout: post
title: 'Kako izabrati agregat i koji agregat kupiti'
date: 2015-04-20 12:00:00
categories: saveti
excerpt_img: 2015/04/kako_izabrati_agregat.jpg
excerpt: Kako izabrati agregat i koji agregat kupiti?
tags: agregat, snaga, cena
---

Neka od glavnih pitanja prilikom izbora agregata:

- Svrha (aplikacija) za koju će se agregat koristiti?
- Potrebna snage agregata?
- Potrebna pouzdanost?
- Način i mesto instalacije (ugradnje) agregata?
- Cena agregata (budžet)?

## Vrsta i snaga agregata

Prvo treba izabrati koja vrsta i snaga agregata Vam je potrebna:

- Monofazni (230 V , 50 Hz): za napajanje samo monofaznih potrošača do 20 kVA
- Trofazni (400 V/230 V, 50 Hz): za napajanje trofaznih i monofaznih potrošača od 3 kVA do 20 kVA
- Prenosni (portable) benzinski ili dizel za snage do 20 kVA
- Stacionarni od 10 kVA do 2000 kVA
- Otvoreni tip agregata – predviđen za unutrašnju montažu
- Kontejnerski tip agregata (canopy) – predviđen za spoljnu montažu a može se montirati i unutar objekta (zaštita od atmosferskih uslova i zaštita od buke)

## Potrebna snaga agregata

Izbor snage agregata je složen proces i vrši se na osnovu proračuna snaga potrošača, tipa potrošača, načina upuštanja svih potrošača koji treba da se napajaju sa agregata kao i proračuna padova napona koje potrošači prouzrokuju prilikom startovanja.

- Ukoliko nema elektromotornog pogona snaga agregata se dobija tako što saberu sve prividne snage potrošača (kVA) doda rezerva od minimalno 10% i izabere prvi veći tipski agregat.
- Ukoliko postoji i elektromotorni pogon vrši se uslovljavanje da se svi elektromotori preko 3 kW upuštaju preko soft startera ili zvezada trougao ili frekventnog regulatora.
- Sada se snaga agregata dobija tako što se saberu sve prividne snage potrošača i doda još dvostruka vrednost prividne snage najvećeg elektromotora i izabere prvi veći standardni agregat.

## Potrebna pouzdanost

U zavisnosti od aplikacije za koju će se koristiti bira se i potrebna pouzdanost agregata.
Ukoliko su potrošači veoma bitini i zahteva se visoka pouzdanost rada agregat moraju se uzeti u obzir i sledeći elementi pri izboru agregata:

- **POGONSKI MOTOR** - Bira se renomiranog evropskog proizvođača sa zastupljenim servisom i rezervnim delovima: PERKINS, FTP IVEKO, VOLVO...
- **GENERATOR** - Bira se renomiranog evropskog proizvođača STANFORD,MARELLI, MECC ALTE, LINZ, SINHRO...)
- **MIKROPROCESORSKI KONTROLER** - za uravljanje radom agregata visoke pouzdanosti onih proizvođača čije kontrolere možete na lageru na našem tržištu kao (COM AP, DSE, DATAKOM...)
- **ATS (Automatic Transfer Switch)** ormana za prebacivanje potrošača mreža / agregat - komponenete renomiranih evropski proizvođača (ABB, SCHRACK,SCHNEIDER,NOARK) kojih ima na lageru na našem tržištu)
- **SERVISNA SLUŽBA** – obučena i opremljena servisna služba za brz odziv 24h sedam dana u nedelji

Dodatni elementi:

- **Redudantni (posebni nezavisni) kontrolno upravljački sistem agregata** - sa nezavisnim davačima koji se koristi u slučaju otkaza osnovnog
- **Orman BY PASS** - nezavisni orman preko koga se može prebaciti napajanje u slučaju otkaza ormana ATS
- **SCADA** – daljinski nadzor i upravljanje radom agregata

## Način i mesto instalacije agregata

Da bi se agregat nesmetano koristio mora da instalira prema zahtevanim uslovima:

- Prostorija u kojoj se smešta agregat i oprema ( dimenzije)
- Provetravanja prostorije – proračun potrebene količine vazduha i izbor najpogodnijeg načina za provetravanje preostorije
- Izrada temelja agregata – proračun i crtež temelja na osnovu izabranog agregata
- Način dopremanja goriva
- Izrada izduvnog sistema (auspuha)

## Cena agregata

U današnje vreme, vreme ekonomske krize CENA AGREGATA je jadan od najbitnijih elemenata pri izboru agregata.

CENA AGREGATA je faktor na osnovu koga treba izabrati agregat ali prvo treba zadovoljiti sve tehničke zahteve agregata za svrhu za koju će se koristiti i onda na osnovu izabrane grupe proizvoda izabrati agregat čija je cena najbolja.
