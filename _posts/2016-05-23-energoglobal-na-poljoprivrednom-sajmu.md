---
layout: post
title:  "Energoglobal na 83. međunarodnom poljoprivrednom sajmu"
date:   2016-05-23 13:37:00
categories: novosti
excerpt_img: 2016/05/sajam.jpg
excerpt: EnergoGlobal je bio izlagač na 83. međunarodnom poljoprivrednom sajmu u Novom Sadu.
tags: agregat, sajam, poljoprivreda
gallery:
  - 2016/05/sajam_0.jpg
  - 2016/05/sajam_1.jpg
  - 2016/05/sajam_2.jpg
  - 2016/05/sajam_3.jpg
  - 2016/05/sajam_4.jpg
  - 2016/05/sajam_5.jpg
---

EnergoGlobal je bio izlagač na 83. međunarodnom poljoprivrednom sajmu u Novom Sadu.
