---
layout: post
title: 'Kako razumeti snagu agregata'
date: 2016-05-27 13:37:00
categories: saveti
excerpt_img: 2016/05/snaga_grafik.jpg
excerpt: Kako razumeti snagu agregata za struju i jedinice mere u kojima se izražavaju
tags: agregat, snaga, kw, kva, kvar
---

Da bi smo u potpunosti razumeli pojam snage kod generatora električne energije neophodno je da razumemo sledeća tri pojma, prividnu snagu S izraženu u KVA, aktivnu snagu P izraženu u kW i reaktivnu snagu Q izraženu u kVAr.

![Grafik: matematički odnos snaga agregata](/assets/img/2016/05/snaga_grafik.jpg){: .img-responsive}

Gde je, dakle P, aktivna snaga, snaga koja vrši koristan rad. Ona predstavlja veći deo prividne snage i troši se na snabdevanje aktivnih potroača, čisto reaktivnih opterećenja (potrošača).

Q je reaktivna snaga, snaga koja se troši na pokrivanje raznih gubitaka unutar samog agregata, ali i za snabdevanje energijom reaktivnih potrošača (magnećenje magnetnih kola pojedinih uređaja, fluorescentna rasveta, asinhroni motori, regulacija napona na transformatorima itd.).

S je prividna snaga, tako reći, ukupna snaga koju generiše naš generator. Ona ima i aktivnu i reaktivnu komponentu, tako pokriva i aktivnu i reaktivnu potrošnju reaktivne energije.
Faktor snage, cosφ, predstavlja odnos aktivne snage i prividne snage, cosφ = P/S.

Analogno ovom objašnjenu posmatraćemo kriglu piva sa slike ispod. Kada naručimo kriglu piva u baru, dobijamo sledeće, kriglu zapremine 0.5 l. Ova zapremina analogna je snazi S, tražili smo 0.5 l i to smo dobili. Unutar krigle postoji tečni deo, deo koji služi za naše uživanje i okrepljenje, kao i kod naših potrošača. To je onaj deo energije koji mi kao konzumenti direktno osećamo i uživamo u njoj. Konačno, do vrha krigle, nalazi se pena, ona je tu da bi ispunila kriglu u potpunosti, u slucaju generatora to je Q, sa njom se pokrivaju gubitci usled prenosa električne energije, vrši se magnećenje transformatora kako bi se na njima održao konstantan napon. Dakle, energija koju ne možemo direktno da osetimo ali svakako postoji i ima svoju namenu, ispunjavajući naše elektroenergetske potrebe, baš kao i pena koja popunjava kriglu.

![Grafik: faktor snage](/assets/img/2016/05/faktor_snage.jpg){: .img-responsive}

**Nenad Santrač**, _dipl. inž. el._

podrska@energoglobal.com
