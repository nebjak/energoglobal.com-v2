---
layout: post
title: '(Video) EnergoGlobal EG 150 P+ / Aerodrom Nikola Tesla'
date: 2016-12-10 13:37:00
categories: reference
excerpt_img: 2016/12/video.jpg
excerpt: (Video) EnergoGlobal EG 150 P+ / Aerodrom Nikola Tesla
tags: agregat, video, aerodrom, perkins
---

EnergoGlobal dizel električni agregat EG 150 P+ obezbeđuje rezervno napajanje novoizgrađene platforme za odleđivanje i sprečavanje zaleđivanja vazduhoplova na aerodromu "Nikola Tesla" u Beogradu.

Agregat je isporučen i pušten u rad u saradnji sa kompanijom Elgra Vision.

<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fenergoglobaldoo%2Fvideos%2F1174622975954131%2F&show_text=1&width=560" width="560" height="438" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>

Više o samom projektu na portalu [Tango Six](http://tangosix.rs/2017/06/01/pustena-u-rad-de-icing-anti-icing-platforma-na-aerodromu-nikola-tesla/)

<br><br><br>
Energoglobal doo vrši prodaju novih, kao i generalno repariranih polovnih dizel električnih agregata. Pored [prodaje dizel agregata](/prodaja-dizel-agregata.html) vršimo i servis što obuhvata hitne intervencije i redovno održavanje. Posedujemo flotu od više agregata različitih snaga i vršimo usluge [rentiranja dizel agregata](/rentiranje-iznajmljivanje-agregata.html) sa mogućnošću instalacije, prevoza na objekat i stalnog prisustva stručnog lica.
