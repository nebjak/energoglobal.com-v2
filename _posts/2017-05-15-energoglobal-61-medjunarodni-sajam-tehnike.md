---
layout: post
title: '(Video) EnergoGlobal na 61. Međunarodnom sajmu tehnike'
date: 2017-05-15 13:37:00
categories: novosti
excerpt_img: 2017/05/video.jpg
excerpt: (Video) EnergoGlobal na 61. Međunarodnom sajmu tehnike
tags: agregat, video, sajam
---

EnergoGlobal je jedan od izlagača na 61. Međunarodnom sajmu tehnike u Beogradu. Pozivamo Vas da nas posetite, Hala 2, štand 2218.

<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fenergoglobaldoo%2Fvideos%2F1320727188010375%2F&show_text=0&width=560" width="560" height="315" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>
