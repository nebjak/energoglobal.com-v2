---
layout: post
title: 'APTIV PAKARD 2 x EG 550 I'
date: 2018-10-03 13:37:00
categories: reference
excerpt_img: 2018/10/aptiv.jpg
excerpt: Isporuka i puštanje u rad dva dizel agregata EG 550 I, svaki snage 550 kVA.
tags: agregat, fpt, linz electric
gallery:
  - 2018/10/aptiv_01.jpg
  - 2018/10/aptiv_02.jpg
  - 2018/10/aptiv_03.jpg
  - 2018/10/aptiv_04.jpg
  - 2018/10/aptiv_05.jpg
---

Isporuka i puštanje u rad dva dizel agregata Energoglobal EG 550 I,
svaki snage 550 kVA / 440 kW, koji će vršiti funkciju rezervnog izvora napajanja u
_novoj fabrici u Leskovcu_ kompanije **APTIV PAKARD**.

Agreagati su u paralelnom - sinhronom radu, što daje ukupnu snagu od 1100 kVA.
Agregati su pogonjeni vrhunskim FPT Industrial (Iveco Motors) motorima.

<br><br><br>
Energoglobal doo vrši prodaju novih, kao i generalno repariranih polovnih dizel električnih agregata. Pored [prodaje dizel agregata](/prodaja-dizel-agregata.html) vršimo i servis što obuhvata hitne intervencije i redovno održavanje. Posedujemo flotu od više agregata različitih snaga i vršimo usluge [rentiranja dizel agregata](/rentiranje-iznajmljivanje-agregata.html) sa mogućnošću instalacije, prevoza na objekat i stalnog prisustva stručnog lica.
