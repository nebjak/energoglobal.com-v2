---
layout: post
title: '(Video) Energoglobal predstavlja novi video spot'
date: 2019-05-20 13:37:00
categories: novosti
excerpt_img: 2019/05/youtube.jpg
excerpt: (Video) Energoglobal predstavlja novi video spot
tags: agregat, video, energoglobal
---

Šta vam je potrebno za vaš uspešan posao?

Pouzdan saradnik kome je stalo do vašeg uspeha, partner koji sluša, razume I deluje.

Gde god da ste!

Kakve god da su vaše potrebe!

Mi imamo rešenje za vas!

<iframe width="640" height="360" src="https://www.youtube.com/embed/wakwYw5vvt4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Naši agregati su dizajnirani tako da pružaju maksimalne performanse za niz različitih aplikacija, što garantuje pouzdano I efikasno napajanje. Sa motorima Perkins, FTP Iveco,Volvo Penta, naši agregati se odlikuju niskim nivoom buke, malom potrošnjom goriva I emisijom izduvnih gasova u skladu sa normama Evropske unije. Oslanjamo se na generatore renomiranih proizvođača koji su prepoznatljivi na svetskom tržištu.
<br>
<br>

**Naše agregate odlikuje:**

- Kompaktan dizajn.
- Automatsko startovanje prilikom prestanka napajanja sa mreže.
- Regulacija napona ispod pola procenta.
- Kućišta od pocinkovanog lima otporna na UV zračenja.
- Lak pristup za dopuni goriva sa spoljne strane.
- Antivibracioni damperi za smanjenje vibracije.
- Atestirane kuke za podizanje agregata I prostor za opsluživanje viljuškarom.
- Lako očitavanje kontrolera.
- Mehnička zaštita od mobilnih I vrelih delova.
- Spoljne konekcije kablovima.
- Mehanički I elektronski indikatori goriva.
- Digitalni regulatori napona ABR.
- Daljinski nadzor I monitoring GSM, GPRS, ETHERNET.
- Istorijat rada,
- Programabilni PLC.
- Produžena garancija od 5 godina

Energoglobal vam nudi još I opcije Super Silent, Paralelni rad dva agregata, Paralelni rad sa mrežom, Peglanje vršnog opterećenja. Mobilne agregate svih snaga

Mi pravimo agregate po meri vaših potreba

<br><br>
Pouzdana energija

**Energoglobal**
